export class Project {
    id: number;
    title: string;
    customer_id: number;
    echance: string;
    description: string;
    full_title: string;
    facturable?: boolean;
    max_hours : number;
    customer_name: string;
    url: string;
    duree_interventions: string;
    created_at: string;
    updated_at: string;
}