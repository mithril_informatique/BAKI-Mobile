export class Sheet {
    id: number;
    start_date: Date;
    duration: number;
    compta?: boolean;
    description: string;
    comment: string;
    user_id: number;
    facturable?: boolean;
    user_name: string;
    customer_id: number;
    customer_name: string;
    url: string;
    state:number;
}