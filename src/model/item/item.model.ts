
export class Article{
    id: number;
    title:string;
    purchase_price:number;
    cost_price :number;
    public_price:number;
    supplier_id:number;
    category_id:number;
    gencode:string;
    description:string;
    can_sell:boolean;
    can_purchase:boolean;
    ref: string;
    url:string;
    purchase_vat_id:number;
    sale_vat_id: number;
    supplier:string;
    supplier_stock:number;
    category:string;
    warranty:number;
    purchase_vat:number;
    sale_vat:number;
    picture:string;
    picture_medium: string;
    picture_thumb:string ;
}