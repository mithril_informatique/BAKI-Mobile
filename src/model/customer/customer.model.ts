
export class Customer {
    id: number;
    name : string;
    odoo_id: number;
    email:string;
    tel : string;
    mobile: string;
    fax: string;
    address: string;
    lat : number;
    long : number;
    notes: string;
    created_at: string;
    updated_at: string;
    url: string;
    supplier:boolean;
    logo_medium: string;
    logo_thumb: string;

}