import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { ArticleListPage } from './../../Article/article-list/article-list';
import { SaleListPage } from './../../Sale/sale-list/sale-list';
import { SheetListPage } from './../../Sheet/sheet-list/sheet-list';
import { CustomersListPage } from './../../Customers/customers-list/customers-list';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PurshaseListPage } from '../../purchase/purshase-list/purshase-list';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
public nomModule:string;
  constructor(public navCtrl: NavController, public menu:MenuController,public alertCtrl: AlertController) {
    this.menu.enable(true, 'myMenu');
  }

  choix(grade): void{

switch (grade) {
   case 1 : {
    this.navCtrl.push(CustomersListPage);
    this.nomModule="Annuaires";
      break;
   }

   case 2 : {
    this.nomModule="interventions";
    this.navCtrl.push(SheetListPage);
    
      break;
   }

   case 3 : {
    this.nomModule="ventes";
    this.showAlert(this.nomModule); 
    //this.navCtrl.push(SaleListPage);
    
      break;
   }
   case 4 : {
    this.nomModule="achats";
    this.showAlert(this.nomModule); 
    //this.navCtrl.push(PurshaseListPage);
      break;
   }
   case 5 : {
    this.nomModule="articles";
    this.navCtrl.push(ArticleListPage);
      break;
   }



}


  }
  showAlert(nomM) {
    let alert = this.alertCtrl.create({
      title: 'module non disponible!',
      subTitle: 'le module '+ nomM+' sera disponible pour la version de 0.4 de Baki_Mobile!',
      buttons: ['OK']
    });
    alert.present();
  }
}
