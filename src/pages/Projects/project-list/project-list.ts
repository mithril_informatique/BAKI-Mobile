import { ProjectFichePage } from './../project-fiche/project-fiche';
import { Projects } from './../../../model/projet/projets.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProjectProvider } from '../../../providers/project/project';

/**
 * Generated class for the ProjectListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-project-list',
  templateUrl: 'project-list.html',
})
export class ProjectListPage {


  Project: Projects = new Projects();
  constructor(public navCtrl: NavController, public navParams: NavParams,private Projectp:ProjectProvider) {
  let id = navParams.get('id');
    this.gePRO(null);
    
  }

  // Chargement des projet
  public gePRO(refresher) {
    this.Projectp.getProject()
    .then(newsFetched => {
      this.Project= newsFetched;




       // Si la variable refresher est null alors on ne fait rien
       (refresher) ? refresher.complete() : null;
       console.log('Données récupérées depuis le serveur !');

     });


   }
   public actionp(id){

    this.navCtrl.push(ProjectFichePage,{id:id});
    }
}
