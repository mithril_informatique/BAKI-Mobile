import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Project } from '../../../model/projet/projet.model';
import marked from 'marked';
import { ProjectProvider } from '../../../providers/project/project';
/**
 * Generated class for the ProjectFichePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-project-fiche',
  templateUrl: 'project-fiche.html',
})
export class ProjectFichePage {
  project: Project = new Project();
  public  markdownText:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public Prop:ProjectProvider) {

    let id = navParams.get('id');
    this.actionpro(id); }


  public actionpro(id){
    this.Prop.getPro(id)
    .then(newsFetched => {
    this.project = newsFetched;
    let plainText = this.project.description;
    this.markdownText = marked(plainText.toString());
 });
  }

}
