import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectFichePage } from './project-fiche';

@NgModule({
  declarations: [
    ProjectFichePage,
  ],
  imports: [
    IonicPageModule.forChild(ProjectFichePage),
  ],
})
export class ProjectFichePageModule {}
