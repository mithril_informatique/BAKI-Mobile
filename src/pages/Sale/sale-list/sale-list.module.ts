import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaleListPage } from './sale-list';

@NgModule({
  declarations: [
    SaleListPage,
  ],
  imports: [
    IonicPageModule.forChild(SaleListPage),
  ],
})
export class SaleListPageModule {}
