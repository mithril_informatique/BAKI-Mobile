import { HomePage } from './../../Autre/home/home';
import { Login } from './../../../model/session/login.model';
import { NativeStorage } from '@ionic-native/native-storage';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { SessionProvider } from '../../../providers/session/session';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { MenuController } from 'ionic-angular/components/app/menu-controller';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  mail : string;
  password : string ;
  profil: Login = new Login();
  logged:number;
  constructor(public navCtrl: NavController, private loginp:SessionProvider,private alertCtrl: AlertController,private nativeStorage: NativeStorage,public toastCtrl: ToastController ,public menu: MenuController) {

    this.menu.enable(false, 'myMenu');}

  showlogin(): void{

   this.loginp.postlLogin(this.mail,this.password)
   .then(newsFetched => {
     this.profil= newsFetched;
      console.log(this.profil);
    if (this.profil.logged == 1)
       {

        this.nativeStorage.setItem('myprofil', {
          logged: this.profil.logged ,
          id : this.profil.id ,
           token:this.profil.token
          })
        .then(
          () => console.log('Stored item!'),
          error => console.error('Error storing item', error)
        );
        this.navCtrl.push(HomePage);
       }
       else
       {

       this.presentToast()
       }
  });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: '!!!!Erreur veulliez recommencer!!!!!!',
      duration: 3000
    });
    toast.present();
  }

}
