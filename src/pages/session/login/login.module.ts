import { SessionProvider } from './../../../providers/session/session';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
   
  
  ],
  exports:[LoginPage],
  providers: [
    SessionProvider,

  ]
})

export class LoginPageModule {}
