import { SheetListPage } from './../sheet-list/sheet-list';
import { Sheet } from './../../../model/sheet/sheet.model.';
import { Projects } from './../../../model/projet/projets.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Project } from '../../../model/projet/projet.model';
import { ProjectProvider } from '../../../providers/project/project';
import { SheetsProvider } from '../../../providers/sheets/sheets';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

/**
 * Generated class for the FormSheetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-sheet',
  templateUrl: 'form-sheet.html',
})
export class FormSheetPage {
  Project: Projects = new Projects();
  project: Project= new Project();
   id_project : number;
   decription :string;
   duree : number;
   date_debut: string  = new Date().toISOString();
   facturable:boolean;
   envoy_mail: boolean;
   fct:boolean;
   titrepage:string;
   sheet :Sheet= new Sheet();
   id: number;
   etat:number;
    constructor(public navCtrl: NavController, private Projectp:ProjectProvider,private sheetp:SheetsProvider,public loadingCtrl: LoadingController, public navParams: NavParams) {
     
      this.etat = navParams.get('etat');
      this.id= navParams.get('id');
       
       if(this.etat == 3) 
       {
        
       //
       } 
       if(this.etat == 2) 
       {
       // 
       } 
       if(this.etat == 1) 
       {
         alert(this.etat);
        this.titrepage = " demande "+this.id;
        this.printsheet(this.id);
       } 
       if (this.etat == null) 
       { 
        this.titrepage = "nouvelle  demande ";
        this.gePRO(null);
        this.vardef(this.id_project); 
       }
   
  }

  ///CREATION INTERVENTION RESQUEST///
    // Chargement des projet
    public gePRO(refresher) {
    this.Projectp.getProject()
    .then(newsFetched => {
      this.Project= newsFetched;







          // Si la variable refresher est null alors on ne fait rien
          (refresher) ? refresher.complete() : null;
          console.log('Données récupérées depuis le serveur !');
        });

      }
    vardef(id_project){
    this.Projectp.getPro(id_project)
    .then(newsFetched => {
      this.project = newsFetched;

    this.fct = this.project.facturable;
  alert(this.fct);
      return this.fct;

    });
  }

      ValideAction(): void{


    console.log(this.facturable);
    let newsheet={'sheet':{'project_id': this.id_project,'start_date': this.date_debut,'duration': this.duree,'description':this.decription ,'facturable': this.facturable,'envoy_mail': this.envoy_mail }};
    console.log(newsheet);
    this.sheetp.postNewSheet(newsheet)

    .then(newsFetched => {
      this.sheet= newsFetched;
        console.log(this.sheet);
        this.sheet= newsFetched;
        this.presentLoading();
        this.navCtrl.push(SheetListPage);
      });

  }

  boutton_retour(): void{

  this.navCtrl.push(SheetListPage);
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
  }
  ////AFFICHAGE DE LA FEUILLE ////

public printsheet(id){
 
    this.sheetp.AfSheet(id)
    .then(newsFetched => {
    this.sheet = newsFetched;
  
   this.decription=this.sheet.description;
   this.duree =this.sheet.duration;
   this.facturable =this.sheet.facturable;
   

 });
  
  
}


////EDITION DE LA  FEUILLE
editeAction(){
  alert("teste");
  
}
 }
