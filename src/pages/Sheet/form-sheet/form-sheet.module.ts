import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormSheetPage } from './form-sheet';

@NgModule({
  declarations: [
    FormSheetPage,
  ],
  imports: [
    IonicPageModule.forChild(FormSheetPage),
  ],
})
export class FormSheetPageModule {}
