import { StatusBar } from '@ionic-native/status-bar';
import { FormSheetPage } from './../form-sheet/form-sheet';
import { Sheets } from './../../../model/sheet/sheets.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SheetsProvider } from '../../../providers/sheets/sheets';

/**
 * Generated class for the SheetListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sheet-list',
  templateUrl: 'sheet-list.html',
})
export class SheetListPage {
  sheet :Sheets= new Sheets();
  public segments:string ="instervention_request";
   constructor(public navCtrl: NavController, public navParams: NavParams, private sheetp:SheetsProvider) {

 this.getSH(null);
 }

 // Chargement des articles
 public getSH(refresher) {
 this.sheetp.getSheet()
 .then(newsFetched => {
   this.sheet= newsFetched;


   console.log(this.sheet);



       // Si la variable refresher est null alors on ne fait rien
       (refresher) ? refresher.complete() : null;
       console.log('Données récupérées depuis le serveur !');
     });

   }

 AlertAction(status,idc): void{

  this.navCtrl.push(FormSheetPage,{etat: status,id: idc});
    }

}
