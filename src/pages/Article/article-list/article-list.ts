import { ArticleFichePage } from './../article-fiche/article-fiche';
import { Articles } from './../../../model/item/items.model.';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ItemProvider } from '../../../providers/item/item';

/**
 * Generated class for the ArticleListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-article-list',
  templateUrl: 'article-list.html',
})
export class ArticleListPage {
articles:Articles =new Articles();
  constructor(public navCtrl: NavController, public navParams: NavParams , private itemar:ItemProvider) {
  this.geArticles(null);
  }
  public geArticles(refresher) {
    this.itemar.getArticles()
    .then(newsFetched => {
      this.articles= newsFetched;
  
  
      console.log(this.articles);
  
  
  
          // Si la variable refresher est null alors on ne fait rien
          (refresher) ? refresher.complete() : null;
          console.log('Données récupérées depuis le serveur !');
         
        });
        
  
      }
      public action(idC){
      this.navCtrl.push(ArticleFichePage,{id: idC});
      }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ArticleListPage');
  }
  
 

}
