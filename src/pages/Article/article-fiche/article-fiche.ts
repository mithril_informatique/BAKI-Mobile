import { Article } from './../../../model/item/item.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ItemProvider } from '../../../providers/item/item';
import { Articles } from '../../../model/item/items.model.';

/**
 * Generated class for the ArticleFichePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-article-fiche',
  templateUrl: 'article-fiche.html',
})
export class ArticleFichePage {
article:Article= new  Article();
Segment: string = "information";

margePourcent:number;
  constructor(public navCtrl: NavController, public navParams: NavParams,private ficheArt:ItemProvider) {
    let id = navParams.get('id');
    this.actionART(id);

  }

  public actionART(id){
    this.ficheArt.getArticle(id)
    .then(newsFetched => {
    this.article = newsFetched;
  
    
  });
}  
 public boolsell(boolsell):string{
   if(boolsell==true){
     return "oui"
   }
   else{
     return"non"
   }
 }
 public boolPurchase(boolpurchase):string{
  if(boolpurchase==true){
    return "oui"
  }
  else{
    return"non"
  }
}
public frais(cout:number,prachat:number):number{
  let i,j:number;
  i= cout - prachat;
  j= Math.round( i*100)/100 ;
  return j;

}
public marge(cout:number,publichat:number):number{
  let i,j:number;
  i= publichat - cout   ;
  j= Math.round( i*100)/100 ;
  return j;
}
public margepourcent(cout:number,publichat:number):number{
  let i,j:number;
 i=this.marge(cout,publichat);
j = (i /publichat)*100;
  return Math.round( j*100)/100 ;
}
}
