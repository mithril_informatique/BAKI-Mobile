import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArticleFichePage } from './article-fiche';

@NgModule({
  declarations: [
    ArticleFichePage,
  ],
  imports: [
    IonicPageModule.forChild(ArticleFichePage),
  ],
})
export class ArticleFichePageModule {}
