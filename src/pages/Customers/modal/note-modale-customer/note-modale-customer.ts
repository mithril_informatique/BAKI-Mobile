import { CustomersProvider } from './../../../../providers/customers/customers';
import { CustomersListPage } from './../../customers-list/customers-list';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CustomerProfilPage } from '../../customer-profil/customer-profil';

/**
 * Generated class for the NoteModaleCustomerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-note-modale-customer',
  templateUrl: 'note-modale-customer.html',
})
export class NoteModaleCustomerPage {

  notes:string;
  id :number;
  constructor(public navCtrl: NavController, public navParams: NavParams,private CL :CustomersProvider) {
    this.id = navParams.get('id');
    this.notes = navParams.get('notes');

  }
valide_boutton(note): void{
  let notes={'notes': note };

  this.CL.putClient(this.id,notes);

  this.navCtrl.push(CustomersListPage);

}
boutton_retour(): void{

this.navCtrl.push(CustomerProfilPage);
}
}
