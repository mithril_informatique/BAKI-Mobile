import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoteModaleCustomerPage } from './note-modale-customer';

@NgModule({
  declarations: [
    NoteModaleCustomerPage,
  ],
  imports: [
    IonicPageModule.forChild(NoteModaleCustomerPage),
  ],
})
export class NoteModaleCustomerPageModule {}
