import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomersListPage } from './customers-list';

@NgModule({
  declarations: [
    CustomersListPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomersListPage),
  ],
})
export class CustomersListPageModule {}
