import { CustomerProfilPage } from './../customer-profil/customer-profil';
import { Customer } from './../../../model/customer/customer.model';
import { CustomersProvider } from './../../../providers/customers/customers';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Customers } from '../../../model/customer/customers.model.';

/**
 * Generated class for the CustomersListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-customers-list',
  templateUrl: 'customers-list.html',
})
export class CustomersListPage {
  customers :Customers = new Customers();
  public segments:string ="customer";
  constructor(public navCtrl: NavController, public navParams: NavParams,private Clientp:CustomersProvider) {
    this.geCL(null);
  }

  // Chargement des articles
  public geCL(refresher) {
  this.Clientp.getClients()
  .then(newsFetched => {
    this.customers= newsFetched;


    console.log(this.customers);



        // Si la variable refresher est null alors on ne fait rien
        (refresher) ? refresher.complete() : null;
        console.log('Données récupérées depuis le serveur !');
      });

    }
    public action(idC){
    this.navCtrl.push(CustomerProfilPage,{id: idC});
    }

  }
