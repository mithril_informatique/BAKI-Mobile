import { Sheets } from './../../../model/sheet/sheets.model';
import { Sheet } from './../../../model/sheet/sheet.model.';
import { ProjectProvider } from './../../../providers/project/project';
import { ProjectListPage } from './../../Projects/project-list/project-list';
import { CustomersProvider } from './../../../providers/customers/customers';
import { Customer } from './../../../model/customer/customer.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import marked from 'marked';
import { NoteModaleCustomerPage } from '../modal/note-modale-customer/note-modale-customer';
import { Projects } from '../../../model/projet/projets.model';
import { SheetsProvider } from '../../../providers/sheets/sheets';
/**
 * Generated class for the CustomerProfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-customer-profil',
  templateUrl: 'customer-profil.html',
})
export class CustomerProfilPage {
  customer :Customer = new Customer();
  Projects: Projects = new Projects();
  sheet:Sheets = new Sheets();
  public localisation :any;
 public urlgeo :string;
 public  markdownText:string;
 public segments:string ="notes";

  constructor(public navCtrl: NavController, public navParams: NavParams, private CL :CustomersProvider,public modalCtrl: ModalController,private Projectp:ProjectProvider,private sheetp:SheetsProvider) {
    let id = navParams.get('id');
    this.actionCL(id);
    this.gePRO(null);

    this.getSH(null);


   }


  public actionCL(id){
    this.CL.getClient(id)
    .then(newsFetched => {
    this.customer = newsFetched;
   
    this.localisation =this.customer.lat+","+this.customer.long;

    let plainText = this.customer.notes;
    this.markdownText = marked(plainText.toString());
   

 });
  } 
   presentModal(idC,noteI) {

    let modal = this.modalCtrl.create(NoteModaleCustomerPage, {id: idC,notes:noteI});
    modal.present();
  }
  
 Geo(id) {
  var uri;  
  var pointName = this.customer.name ;  
  var coords = this.customer.lat + ',' + this.customer.long;
  uri = 'geo:0,0?q=' + coords + '(' + pointName + ')';


  window.open(uri);  

}

  public actionproject(idp){
    this.navCtrl.push(ProjectListPage,{id: idp});
    }

    public gePRO(refresher) {
      this.Projectp.getProject()
      .then(newsFetched => {
        this.Projects= newsFetched;
  
  
  
  
         // Si la variable refresher est null alors on ne fait rien
         (refresher) ? refresher.complete() : null;
         console.log('Données récupérées depuis le serveur !');
  
       });
  
  
     }
     public getSH(refresher) {
      this.sheetp.getSheet()
      .then(newsFetched => {
        this.sheet= newsFetched;
     
     
        console.log(this.sheet);
     
     
     
            // Si la variable refresher est null alors on ne fait rien
            (refresher) ? refresher.complete() : null;
            console.log('Données récupérées depuis le serveur !');
          });
     
        }   
}

