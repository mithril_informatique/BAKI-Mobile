import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomerProfilPage } from './customer-profil';

@NgModule({
  declarations: [
    CustomerProfilPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomerProfilPage),
  ],
})
export class CustomerProfilPageModule {}
