import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PurshaseListPage } from './purshase-list';

@NgModule({
  declarations: [
    PurshaseListPage,
  ],
  imports: [
    IonicPageModule.forChild(PurshaseListPage),
  ],
})
export class PurshaseListPageModule {}
