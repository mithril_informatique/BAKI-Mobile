import { Project } from './../../model/projet/projet.model';
import { Projects } from './../../model/projet/projets.model';
import { Http } from '@angular/http';
import { Injectable }   from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
// RxJS
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
/*
  Generated class for the CustomerProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
 

@Injectable()
export class ProjectProvider {

  
    private baseUrl: string = 'https://fdt.mithril.re/projects';
    private apiKey: string ;
  
    constructor(private http: Http,private nativeStorage: NativeStorage) {
    
    }  
    
        public getProject():Promise<any>{  
          this.nativeStorage.getItem('myprofil')
          .then(
            data => {this.apiKey = data.token},
            error => console.error(error)
          );
        const url = `${this.baseUrl}.json?user_credentials=${this.apiKey}`;
        
        
        return this.http.get(url)
        .toPromise()
        .then(response => response.json() as Projects)
        .catch(error => console.log('Une erreur est survenue ' + error))
    }
  
    public getPro(idP):Promise<any>{  
      const url = `${this.baseUrl}/${idP}.json?user_credentials=${this.apiKey}`;
      return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Project)
      .catch(error => console.log('Une erreur est survenue ' + error))
    }
  
  }
  
      
  
  
  
