import { Article } from './../../model/item/item.model';
import { Articles } from './../../model/item/items.model.';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { NativeStorage } from '@ionic-native/native-storage';
import { Item } from 'ionic-angular';

/*
  Generated class for the ItemProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ItemProvider {

  private baseUrl: string = 'https://fdt.mithril.re/articles';
  private apiKey: string ;
  private data =' ';
  constructor(public http: Http,private nativeStorage: NativeStorage) {
  
  }  
  public getArticles():Promise<any>{  
    this.nativeStorage.getItem('myprofil')
    .then(
      data => {this.apiKey = data.token},
      error => console.error(error)
    );
    const url = `${this.baseUrl}.json?user_credentials=${this.apiKey}`;
  
  
    return this.http.get(url)
    .toPromise()
    .then(response => response.json() as Articles)
    .catch(error => console.log('Une erreur est survenue ' + error))
  }
  
  
  
  
  public getArticle(id):Promise<any>{  
    const url = `${this.baseUrl}/${id}.json?user_credentials=${this.apiKey}`;
    
    
    return this.http.get(url)
    .toPromise()
    .then(response => response.json() as Article)
    .catch(error => console.log('Une erreur est survenue ' + error))
  }
}
