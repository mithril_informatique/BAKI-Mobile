import { Login } from './../../model/session/login.model';

import { Http } from '@angular/http';



import { Injectable }   from '@angular/core';


// RxJS
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
/*
  Generated class for the SessionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SessionProvider {

 
  private baseUrl: string = 'https://fdt.mithril.re/get_token.json';

  
  constructor(private http: Http) {
  
  }  
  
   public postlLogin(apiKey1,apiKey2):any{  
     
    console.log(apiKey1);
    console.log(apiKey2);
    const url = `${this.baseUrl}?email=${apiKey1}&password=${apiKey2}`;
      let data ='';
      
      return this.http.post(url,data)
      .toPromise()
      
      .then(response => response.json() as Login)
      .catch(error => console.log('Une erreur est survenue ' + error))
      
  }

}
