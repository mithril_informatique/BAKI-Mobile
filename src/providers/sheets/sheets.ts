import { Sheet } from './../../model/sheet/sheet.model.';
import { Sheets } from './../../model/sheet/sheets.model';
import { Http ,Headers, RequestOptions} from '@angular/http';
import { Injectable }   from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
// RxJS
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

/*
  Generated class for the SheetsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SheetsProvider {

  
  private baseUrl: string = 'https://fdt.mithril.re/sheets.json';
  private apiKey: string ;
  private data =' ';
  constructor(private http: Http,private nativeStorage: NativeStorage) {
  
  }  
  
  public getSheet():Promise<any>{  
    this.nativeStorage.getItem('myprofil')
    .then(
      data => {this.apiKey = data.token},
      error => console.error(error)
    );
  const url = `${this.baseUrl}?user_credentials=${this.apiKey}`;
  
  
  return this.http.get(url)
  .toPromise()
  .then(response => response.json() as Sheets)
  .catch(error => console.log('Une erreur est survenue ' + error))
}

public postNewSheet(data):Promise<Sheet>{  

console.log(this.apiKey);

const url = `${this.baseUrl}?user_credentials=${this.apiKey}`;

 let headers = new Headers({ 'Content-Type': 'application/json' });
 let options = new RequestOptions({ headers: headers });
 console.log(url);
 console.log(data);
  console.log(options);
 return this.http.post(url,data,options)
 .toPromise()
 .then(response => response.json() )
 .catch(error => alert('Une erreur est survenue ' + error))
 
}
public  AfSheet(id):Promise<any>{  
const url = `${this.baseUrl}/${id}.json?user_credentials=${this.apiKey}`;


return this.http.get(url)
.toPromise()
.then(response => response.json() as Sheet)
.catch(error => console.log('Une erreur est survenue ' + error))
}





public putSheet(id,data):Promise<Sheet>{  
const url = `${this.baseUrl}/${id}.json?user_credentials=${this.apiKey}`;

let headers = new Headers({ 'Content-Type': 'application/json' });
let options = new RequestOptions({ headers: headers });
return this.http.put(url,data,options)
.toPromise()
.then(response => response.json() )
.catch(error => alert('Une erreur est survenue ' + error))

}
}
