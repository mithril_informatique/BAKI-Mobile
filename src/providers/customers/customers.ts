import { Customer } from './../../model/customer/customer.model';
import { Customers } from './../../model/customer/customers.model.';

import { Http ,Headers, RequestOptions} from '@angular/http';



import { Injectable }   from '@angular/core';


// RxJS
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { NativeStorage } from '@ionic-native/native-storage';
/*
  Generated class for the CustomerProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
 
@Injectable()
export class CustomersProvider {

  private baseUrl: string = 'https://fdt.mithril.re/customers';
  private apiKey: string ;
  private data =' ';
  constructor(private http: Http,private nativeStorage: NativeStorage ){
  
  }  
  
  public getClients():Promise<any>{  
    this.nativeStorage.getItem('myprofil')
    .then(
      data => {this.apiKey = data.token},
      error => console.error(error)
    );
    const url = `${this.baseUrl}.json?user_credentials=${this.apiKey}`;
  
  
    return this.http.get(url)
    .toPromise()
    .then(response => response.json() as Customers)
    .catch(error => console.log('Une erreur est survenue ' + error))
  }
  
  
  
  
  public getClient(id):Promise<any>{  
    const url = `${this.baseUrl}/${id}.json?user_credentials=${this.apiKey}`;
    
    
    return this.http.get(url)
    .toPromise()
    .then(response => response.json() as Customer)
    .catch(error => console.log('Une erreur est survenue ' + error))
  }


    


public putClient(id,data):Promise<Customer>{  
    const url = `${this.baseUrl}/${id}.json?user_credentials=${this.apiKey}`;
    
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.put(url,data,options)
    .toPromise()
    .then(response => response.json() )
    .catch(error => alert('Une erreur est survenue ' + error))
    
}
}
