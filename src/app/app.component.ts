import { NativeStorage } from '@ionic-native/native-storage';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/Autre/home/home';
import { ListPage } from '../pages/Autre/list/list';

import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { ProjectListPage } from '../pages/Projects/project-list/project-list';
import { AproposPage } from '../pages/Autre/apropos/apropos';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'LoginPage';

  pages: Array<{title: string, component: any}>;

  public idt: number;
  public token: string;
  public LogOK: number;
   public loginprofil:string;
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public native: NativeStorage,public menu: MenuController) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Accueil', component: HomePage },
      { title: 'Projets/Contrats', component: ProjectListPage},
      { title: 'A propos', component: AproposPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  deconnection():void {
    this.native.remove('myprofil');
    this.nav.setRoot('LoginPage');

  }
}
