import { FormSheetPage } from './../pages/Sheet/form-sheet/form-sheet';
import { NoteModaleCustomerPage } from './../pages/Customers/modal/note-modale-customer/note-modale-customer';
import { SheetsProvider } from './../providers/sheets/sheets';
import { CustomersProvider } from './../providers/customers/customers';


import { SheetListPage } from './../pages/Sheet/sheet-list/sheet-list';
import { SaleListPage } from './../pages/Sale/sale-list/sale-list';
import { ProjectFichePage } from './../pages/Projects/project-fiche/project-fiche';
import { ProjectListPage } from './../pages/Projects/project-list/project-list';
import { ArticleFichePage } from './../pages/Article/article-fiche/article-fiche';
import { ArticleListPage } from './../pages/Article/article-list/article-list';

import { LoginPage } from './../pages/session/login/login';


import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/Autre/home/home';
import { ListPage } from '../pages/Autre/list/list';
import { AproposPage } from './../pages/Autre/apropos/apropos';
import { CustomerProfilPage } from './../pages/Customers/customer-profil/customer-profil';
import { CustomersListPage } from './../pages/Customers/customers-list/customers-list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProjectProvider } from '../providers/project/project';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpModule } from '@angular/http';
import { ItemProvider } from '../providers/item/item';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    AproposPage,
    CustomerProfilPage,
    CustomersListPage,
   
    SheetListPage,
 
    ArticleListPage,
    ArticleFichePage,
    ProjectListPage,
    ProjectFichePage,
    SaleListPage,
    NoteModaleCustomerPage,
    FormSheetPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    CustomerProfilPage,
    CustomersListPage,
    SheetListPage,
  
    ArticleListPage,
    ArticleFichePage,
    ProjectListPage,
    ProjectFichePage,
    SaleListPage,
    NoteModaleCustomerPage,
    AproposPage,
    FormSheetPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  
    CustomersProvider,
    SheetsProvider,
    ProjectProvider,
    NativeStorage,
    ItemProvider

  ]
})
export class AppModule {}
